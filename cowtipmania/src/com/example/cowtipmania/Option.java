package com.example.cowtipmania;


import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Option extends Activity {
	public int SCREEN_HEIGHT = 0;
	public int SCREEN_WIDTH = 0;
	static final String CHECKBOX_BOOL = "checkBoxBool";
	static final String SOUNDCHECKBOX_BOOL = "soundCheckBoxBool";
	CheckBox checkbox;
	CheckBox soundCheckBox;
	public static final String PREFERENCES_NAME = "myPreferences";
	public boolean soundEnabled;
	protected static int windowFocus = 0;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //Find the screen dimensions for landscape mode
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        SCREEN_HEIGHT = size.y;
        SCREEN_WIDTH = size.x;
        
        //Intent intent = getIntent();
        //Set over-arching layout container
        RelativeLayout layoutOption = new RelativeLayout(this);
        layoutOption.setId(106);
        RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(
        		LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        layoutOption.setLayoutParams(llp);
        
        
        //New layoutparams for title
        RelativeLayout.LayoutParams mlp = new RelativeLayout.LayoutParams(
        		LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mlp.addRule(RelativeLayout.CENTER_IN_PARENT);  
         
        //Set textview to display title
        TextView title = new TextView(this);
        title.setId(102);
        title.setText(R.string.textview_option);
        title.setTextSize((int)(SCREEN_HEIGHT / 20));
        title.setLayoutParams(mlp);
        layoutOption.addView(title);
        
        //New layoutparams for container
        RelativeLayout.LayoutParams wlp = new RelativeLayout.LayoutParams(
        		LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        wlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        
        //New container
        RelativeLayout container = new RelativeLayout(this);
        container.setLayoutParams(wlp);
        layoutOption.addView(container);
        
        //New layoutparams for checkbox
        RelativeLayout.LayoutParams xlp = new RelativeLayout.LayoutParams(
        		LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        xlp.addRule(RelativeLayout.CENTER_IN_PARENT);
        
        //New layoutparams for soundCheckBox
        RelativeLayout.LayoutParams ylp = new RelativeLayout.LayoutParams(
        		LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        ylp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        
        
        //New checkbox for music option
        checkbox = new CheckBox(this);
        checkbox.setId(111);
        checkbox.setText(R.string.checkbox_musicoff);
        checkbox.setTextSize(30);
        checkbox.setLayoutParams(xlp);
        
        //New checkbox for sound option
        soundCheckBox = new CheckBox(this);
        soundCheckBox.setText(R.string.checkbox_soundoff);
        soundCheckBox.setTextSize(30);
        soundCheckBox.setLayoutParams(ylp);
               
        //Restores the state of the checkbox
        if(savedInstanceState != null){
      	  checkbox.setChecked(savedInstanceState.getBoolean(CHECKBOX_BOOL));
      	  soundCheckBox.setChecked(savedInstanceState.getBoolean(SOUNDCHECKBOX_BOOL));
      	  MusicMethod.musicStopped = checkbox.isChecked();
      	  MusicMethod.soundFXStopped = soundCheckBox.isChecked();
        }
        else{
        	checkbox.setChecked(false);
        	soundCheckBox.setChecked(false);
        	MusicMethod.musicStopped = checkbox.isChecked();
        	MusicMethod.soundFXStopped = soundCheckBox.isChecked();
        }
        
        //Load checkbox state saved from onBack() call (back button)
        LoadPreferences();
        
        //Adds the checkbox to the container
        container.addView(checkbox);
        container.addView(soundCheckBox);
        
        
        checkbox.setOnClickListener(new View.OnClickListener() {
    		@Override
        	public void onClick(View view){
        		if(checkbox.isChecked()){
        			MusicMethod.suffolkShuffle.pause();
        			MusicMethod.musicStopped = true;
        		}
        		else if(!checkbox.isChecked()){
        			MusicMethod.suffolkShuffle.start();
        			MusicMethod.musicStopped = false;
        		}
        	}
        });
        
        soundCheckBox.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				if(soundCheckBox.isChecked()){
					MusicMethod.soundFXStopped = true;
				}
				else if(!soundCheckBox.isChecked()){
					MusicMethod.soundFXStopped = false;
				}
				
			}
		});
        
        //Set display
        setContentView(layoutOption, llp);

    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus){
    	
    	if(hasFocus)
    		windowFocus = 1;
    	else
    		windowFocus = 0;	 	  
    	
 	   	MusicMethod.calculateFocusTotal();
 	   
 	   super.onWindowFocusChanged(hasFocus);
    }
    
    //This method is overridden so the current state of the checkbox
    //can be saved
    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState){
      //Saves state of checkbox
      savedInstanceState.putBoolean(CHECKBOX_BOOL, checkbox.isChecked());
      savedInstanceState.putBoolean(SOUNDCHECKBOX_BOOL, soundCheckBox.isChecked());
      MusicMethod.musicStopped = checkbox.isChecked(); //Sets a flag for use on MainActivity exit
      MusicMethod.soundFXStopped = soundCheckBox.isChecked();
    	
      super.onSaveInstanceState(savedInstanceState);
      
    }
    
    //This method is overridden so the state of the checkbox is restored
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
      super.onRestoreInstanceState(savedInstanceState);
   
      //Restores the state of the checkbox
      checkbox.setChecked(savedInstanceState.getBoolean(CHECKBOX_BOOL));
      soundCheckBox.setChecked(savedInstanceState.getBoolean(SOUNDCHECKBOX_BOOL));
      MusicMethod.musicStopped = checkbox.isChecked();
      MusicMethod.soundFXStopped = soundCheckBox.isChecked();
      
    }
  
    //This following are used to save and load state when the back button is clicked
    private void SavePreferences(){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("checkBoxBool", checkbox.isChecked());
        editor.putBoolean("soundCheckBoxBool", soundCheckBox.isChecked());
        MusicMethod.musicStopped = checkbox.isChecked();
        MusicMethod.soundFXStopped = soundCheckBox.isChecked();
        editor.commit();
       }

       private void LoadPreferences(){
        SharedPreferences sharedPreferences = getPreferences(MODE_PRIVATE);
        Boolean  checkBoxBool = sharedPreferences.getBoolean("checkBoxBool", false);
        Boolean soundCheckBoxBool = sharedPreferences.getBoolean("soundCheckBoxBool", false);
        checkbox.setChecked(checkBoxBool);
        soundCheckBox.setChecked(soundCheckBoxBool);
        MusicMethod.musicStopped = checkbox.isChecked();
        MusicMethod.soundFXStopped = soundCheckBox.isChecked();
       }
  
    //Saves current checkbox state when back button is pressed
    @Override
    public void onBackPressed() {
        //Save current checkbox state
        SavePreferences();
        super.onBackPressed();
       
    }

}