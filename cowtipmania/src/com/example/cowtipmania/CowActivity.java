package com.example.cowtipmania;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class CowActivity extends Activity{
	
	private CowTipView myCowTipView;
	public static final String PREFERENCES_NAME = "myPreferences";
	public boolean soundEnabled;
	protected static int windowFocus = 0;

	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // Lose the navigation bar and make the game full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  WindowManager.LayoutParams.FLAG_FULLSCREEN);
        
        setContentView(R.layout.cowtipping_layout);
        
        myCowTipView = (CowTipView) findViewById(R.id.cow);
        
        myCowTipView.setKeepScreenOn(true);   
        SharedPreferences settings = getSharedPreferences(PREFERENCES_NAME, 0);
        soundEnabled = settings.getBoolean("soundSetting", true);
        CowTipView.soundOn = soundEnabled;
        
	}
	
    @Override
    public void onWindowFocusChanged(boolean hasFocus){
    	
    	if(hasFocus)
    		windowFocus = 1;
    	else
    		windowFocus = 0;	 	  
    	
 	   	MusicMethod.calculateFocusTotal();
 	   
 	   super.onWindowFocusChanged(hasFocus);
    }
}