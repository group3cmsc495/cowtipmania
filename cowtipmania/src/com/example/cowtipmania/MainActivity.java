package com.example.cowtipmania;

import android.annotation.SuppressLint;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

@SuppressLint("NewApi") public class MainActivity extends Activity {
	static final String MUSIC_BOOL = "musicBool";
	MediaPlayer suffolkShuffle;
	public int SCREEN_HEIGHT = 0;
	public int SCREEN_WIDTH = 0;	
	public static final String PREFERENCES_NAME = "myPreferences";
	protected static int windowFocus = 0;
	private static int counter = 0;
 
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
               
        //Find the screen dimensions for landscape mode
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        SCREEN_HEIGHT = size.y;
        SCREEN_WIDTH = size.x;
        
	 // Lose the navigation bar and make the game full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //Set overarching layout container
        RelativeLayout layout1 = new RelativeLayout(this);
        layout1.setId(101);
        RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(
        		LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        layout1.setLayoutParams(llp);
        
        
        //New layoutparams for title
        RelativeLayout.LayoutParams mlp = new RelativeLayout.LayoutParams(
        		LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mlp.addRule(RelativeLayout.CENTER_IN_PARENT);    
     
        //Set textview to display title
        TextView title = new TextView(this);
        title.setId(102);
        title.setText(R.string.textview_cowtipmania);
        title.setTextSize((int)(SCREEN_HEIGHT / 20));
        title.setLayoutParams(mlp);
        layout1.addView(title);
        
        //New layoutparams for start button
        RelativeLayout.LayoutParams nlp = new RelativeLayout.LayoutParams(
        		LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        nlp.addRule(RelativeLayout.CENTER_IN_PARENT);
        nlp.addRule(RelativeLayout.BELOW, title.getId());
        
        
        //Start button
        Button startButton = new Button(this);
        startButton.setClickable(true);
        startButton.setBackgroundColor(Color.TRANSPARENT);
        startButton.setText(R.string.button_start);

        startButton.setTextSize((int)(SCREEN_HEIGHT / 25));
        startButton.setLayoutParams(nlp);
        layout1.addView(startButton);
        
        //Set onClick for Start Button
        startButton.setOnClickListener(new View.OnClickListener() {
    		@Override
        	public void onClick(View view){
        		startGame(view);
        	}
        });
        
        //New layoutparams for quitAndOptionsContainer
        RelativeLayout.LayoutParams olp = new RelativeLayout.LayoutParams(
        		LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        olp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        
        //Create quitAndOptionsContainer
        RelativeLayout quitAndOptionsContainer = new RelativeLayout(this);
        layout1.setId(103);
        quitAndOptionsContainer.setLayoutParams(olp);
        layout1.addView(quitAndOptionsContainer);
        
        //New layoutparams for options button
        RelativeLayout.LayoutParams plp = new RelativeLayout.LayoutParams(
        		LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        plp.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        
        //Options button
        Button optionsButton = new Button(this);
        optionsButton.setBackgroundColor(Color.TRANSPARENT);
        optionsButton.setText(R.string.button_options);
        optionsButton.setTextSize((int)(SCREEN_HEIGHT / 30));
        optionsButton.setLayoutParams(plp);
        quitAndOptionsContainer.addView(optionsButton);
        
        //Set onClick for Options Button
        optionsButton.setOnClickListener(new View.OnClickListener() {
    		@Override
        	public void onClick(View view){
        		setOptions(view);
        	}
        });
        
        //Add a layoutparams for the quit button
        RelativeLayout.LayoutParams qlp = new RelativeLayout.LayoutParams(
        		LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        qlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                
        //Quit button
        Button quitButton = new Button(this);
        quitButton.setBackgroundColor(Color.TRANSPARENT);
        quitButton.setText(R.string.button_quit);
        quitButton.setTextSize((int)(SCREEN_HEIGHT / 30));
        quitButton.setLayoutParams(qlp);
        quitAndOptionsContainer.addView(quitButton);
        
        //Set onClick for Quit Button
        quitButton.setOnClickListener(new View.OnClickListener() {
    		@Override
        	public void onClick(View view){
    			MusicMethod.suffolkShuffle.release();
    			System.exit(0);//Leave this in or there will be an error when quit is clicked.
    			finish();
        	}
        });
        
        
        //Set display
        setContentView(layout1, llp);
        
    }
   
    //Switches to the StartGame.class
    public void startGame(View view){
    	Intent intent = new Intent(this, CowActivity.class);
    	startActivity(intent);
    }
    
    //Switches to the Option.class
    public void setOptions(View view){
    	Intent intent = new Intent(this, Option.class);
    	startActivity(intent);
    }
    
    @Override
    public void onWindowFocusChanged(boolean hasFocus){
    	
    	if(hasFocus)
    		windowFocus = 1;//Indicates MainActivity has the focus
    	else
    		windowFocus = 0;

 	   	if(counter == 0){
    		MusicMethod.SoundPlayer(this, R.raw.anw2064_03_suffolk_shuffle);
    		MusicMethod.suffolkShuffle.start();
 	   	}
 	   	
 	   	if(counter != 0)
 	   		MusicMethod.calculateFocusTotal();
 	   	
    	++counter; 	  
 	   
 	   super.onWindowFocusChanged(hasFocus);
    }

}
    
