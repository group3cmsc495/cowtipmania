package com.example.cowtipmania;


import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StartGame extends Activity {
	public int SCREEN_HEIGHT = 0;
	public int SCREEN_WIDTH = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        //Find the screen dimensions for landscape mode
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        SCREEN_HEIGHT = size.y;
        SCREEN_WIDTH = size.x;
        
        //Intent intent = getIntent();
        //Set over-arching layout container
        RelativeLayout layoutStartGame = new RelativeLayout(this);
        layoutStartGame.setId(106);
        RelativeLayout.LayoutParams llp = new RelativeLayout.LayoutParams(
        		LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        layoutStartGame.setLayoutParams(llp);
        
        
        //New layoutparams for title
        RelativeLayout.LayoutParams mlp = new RelativeLayout.LayoutParams(
        		LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mlp.addRule(RelativeLayout.CENTER_IN_PARENT);    
     
        //Set textview to display title
        TextView title = new TextView(this);
        title.setId(102);
        title.setText(R.string.textview_startgame);
        title.setTextSize((int)(SCREEN_HEIGHT / 20));
        title.setLayoutParams(mlp);
        layoutStartGame.addView(title);
        
        //Set display
        setContentView(layoutStartGame, llp);

    }

}