package com.example.cowtipmania;




import java.util.Random;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.ImageView;


public class CowTipView extends SurfaceView implements SurfaceHolder.Callback{
	
	private Context myContext;
	private SurfaceHolder mySurfaceHolder;
	private Bitmap backgroundImg;
	private int screenW = 1;
	private int screenH = 1;
	private boolean running = false;
	private boolean onTitle = false;
	private CowTippingThread thread;
	public AnimationDrawable run_cow;
	public ImageView cow_walking;
	private Bitmap cow;
	private Bitmap whack;
	
	private float drawScaleW;
	private boolean gameover = false;
	private int cowsmissed = 0;
	private int cowstipped = 0;
	private Paint blackPaint;
	private boolean cowwalking = true;
	private boolean cowtipped = false;
	private boolean cowtipping = false;
	private int cowX;
	private int cowY;
	
	private int walk_speed = 5;
	private int fingerX, fingerY;
	public static boolean soundOn;
	private static SoundPool sounds;
    private static int mooSound;
    private static int missSound;


	public CowTipView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		
		thread = new CowTippingThread(holder, context, new Handler(){
			public void handleMessage(Message m){
				
			}
			
		});
		setFocusable(true);
	}
	
	public CowTippingThread getThread(){
		return thread;
	}
	
	class CowTippingThread extends Thread{
		public CowTippingThread(SurfaceHolder surfaceHolder, Context context, Handler handler){
			mySurfaceHolder = surfaceHolder;
			myContext = context;
			
			// create a soundpool object to play game sounds
    		sounds = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
    		mooSound = sounds.load(myContext, R.raw.moo, 1);
    		missSound = sounds.load(myContext, R.raw.miss, 1);
    		
    		// create a bitmap for the background and graphics
    		backgroundImg = BitmapFactory.decodeResource(myContext.getResources(), R.drawable.background);
			backgroundImg = Bitmap.createScaledBitmap(backgroundImg, screenW, screenH, true);
			
			cow = BitmapFactory.decodeResource(myContext.getResources(), R.drawable.walk_1);
			whack = BitmapFactory.decodeResource(myContext.getResources(), R.drawable.whack);
			cow = Bitmap.createBitmap(cow);
			whack=Bitmap.createBitmap(whack);
			

		}
		
		@Override
		public void run(){
			while(running){
				Canvas c = null;
				try{
					c=mySurfaceHolder.lockCanvas(null);
					synchronized(mySurfaceHolder){
						if (!gameover) {
                        	walkcows();                    		
                    	}
						draw(c);
						
					}
				} finally{
					if (c!=null){
						mySurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}
		
		private void draw(Canvas canvas){
			try{
				canvas.drawBitmap(backgroundImg, 0, 0, null);
				canvas.drawText("Title: " + getTitle(), screenW/4, blackPaint.getTextSize()+10, blackPaint);
				canvas.drawText("Missed: " + Integer.toString(cowsmissed), 10, blackPaint.getTextSize()+10, blackPaint);           		
				canvas.drawText("Tipped: " + Integer.toString(cowstipped), screenW-(int)(300*drawScaleW), blackPaint.getTextSize()+10, blackPaint);
				canvas.drawBitmap(cow, cowX, cowY, null);
				
	
					
				
				if (cowtipping) {
                	canvas.drawBitmap(whack, fingerX - (whack.getWidth()/2), fingerY - (whack.getHeight()/2), null);	
                }

				
                if (gameover) {
                	canvas.drawText("GAME OVER!", (float)screenW/3, (float)screenH/3, blackPaint);
                	canvas.drawText("You are a " + getTitle(), (float)screenW/3, (screenH/2)-20, blackPaint);
                }
                
                				
			} catch (Exception e){				
			}
		}
		
		boolean doTouchEvent(MotionEvent event){
			synchronized(mySurfaceHolder){
				int eventaction = event.getAction();
				int X = (int)event.getX();
				int Y = (int)event.getY();
				
				switch(eventaction){
				
				case MotionEvent.ACTION_DOWN:
					if (!gameover) {
        	        	fingerX = X;
        	        	fingerY = Y;
        	        	if (!onTitle && detectCowStrike()) {
        	        		cowtipping = true;
        	        		if (!MusicMethod.soundFXStopped) {
            	        		AudioManager audioManager = (AudioManager) myContext.getSystemService(Context.AUDIO_SERVICE);
            	        		float volume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
            			        sounds.play(mooSound, volume, volume, 1, 0, 1);        	        		
            	        	}
        	        		cowstipped++;
        	        	}				
					}
					break;
				
				case MotionEvent.ACTION_MOVE:
					break;
					
				case MotionEvent.ACTION_UP:
										
					cowtipping = false;
	
					break;
				}
			}
			return true;
		}
		
		private boolean detectCowStrike() {
			boolean contact = false;
			
			if((fingerX >= cowX) && (fingerY >= cowY) && (fingerX < (cowX+cow.getWidth()))&& (fingerY < (cowY+cow.getHeight()))){
				contact = true;
				cowtipped = true;
			}
			
			return contact;
		}

		public void setSurfaceSize(int width, int height){
			synchronized(mySurfaceHolder){
				screenW = width;
				screenH = height;
				drawScaleW = (float) screenW / 800;
                cowX = (screenW);
                cowY = (screenH/2 - 75);
                backgroundImg = BitmapFactory.decodeResource(myContext.getResources(), R.drawable.background);
				backgroundImg = Bitmap.createScaledBitmap(backgroundImg, width, height, true);
				blackPaint = new Paint();
        		blackPaint.setAntiAlias(true);
        		blackPaint.setColor(Color.BLACK);
        		blackPaint.setStyle(Paint.Style.FILL);
        		blackPaint.setTextAlign(Paint.Align.LEFT);
        		blackPaint.setTextSize(drawScaleW*40);
			}
		}
		
		public void setRunning(boolean b){
			running = b;
		}
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event){
		return thread.doTouchEvent(event);
	}
	
	public void walkcows() {
		
		if(cowwalking){
			cowX-=walk_speed;
		}
		if((cowX<=0) || cowtipped){
			
			start_cow();
		}
		
	}

	private void start_cow() {
		
		if(!cowtipped){
			if (!MusicMethod.soundFXStopped) {
				AudioManager audioManager = (AudioManager) myContext.getSystemService(Context.AUDIO_SERVICE);
	        	float volume = (float) audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
				sounds.play(missSound, volume, volume, 1, 0, 1);					
			}
			cowsmissed++;
			if (cowsmissed > 4) {
				gameover = true;
			}
		}
		cowtipped=false;
		cowX = (screenW);
		
		cowY = new Random().nextInt(screenH-cow.getHeight());
		walk_speed = walk_speed + (int)cowstipped/2;
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		thread.setSurfaceSize(width, height);
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		thread.setRunning(true);
		if(thread.getState() == Thread.State.NEW){
			thread.start();
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		thread.setRunning(false);
		
	}
	
	public String getTitle()
	{
		if(cowstipped < 5)
		{
			return "Beginner";
		}
		else if(cowstipped <= 10)
		{
			return "Apprentice";
		}
		else if(cowstipped <= 15)
		{
			return "Farm Hand";
		}
		else if(cowstipped <= 20)
		{
			return "Cow Herder";
		}
		else if(cowstipped <= 25)
		{
			return "Shepherd";
		}
		else
		{
			return "Master";
		}
	}

}








