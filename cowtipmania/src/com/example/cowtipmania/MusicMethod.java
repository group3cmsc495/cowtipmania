package com.example.cowtipmania;

import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import android.media.MediaPlayer;

public class MusicMethod {
	public static boolean musicStopped = false; //used to save state
	public static boolean soundFXStopped = false; //used to save state
	public static MediaPlayer suffolkShuffle;
	protected static Timer timer;
	
	//Timer used to check if the app went to the background or
	//just switched to another screen.  If it went to the 
	//background it will pause the music.
	static class MusicTimerTask extends TimerTask {
		public void run(){
			int total = 0;

			//Total the focus
			total += MainActivity.windowFocus;
	 	   	total += Option.windowFocus;
	 	   	total += CowActivity.windowFocus;
	 	   	
	 	   	
	 	   	if(total == 0){
	 	   		MusicMethod.suffolkShuffle.pause();
	 	   	}
	 	   	else if(total == 1 && (!MusicMethod.musicStopped) && (!MusicMethod.suffolkShuffle.isPlaying()))
	 	   		MusicMethod.suffolkShuffle.start();
	 	   
	 	   	timer.cancel();
		}
	}
	
	//Used to initialize the media player
    public static void SoundPlayer(Context ctx,int raw_id){
        suffolkShuffle = MediaPlayer.create(ctx, raw_id);
        suffolkShuffle.setLooping(true); // Set looping
        suffolkShuffle.setVolume(10, 10);
    }
    
    //Used to calculate if the current screen is switching
    //If so, and none of the in game screens are the focus
    //it uses the timer to wait and see if it is only 
    //switching to another screen or not.
	protected static void calculateFocusTotal(){
		
		int total = 0;

		//Total the focus
		total += MainActivity.windowFocus;
 	   	total += Option.windowFocus;
 	   	total += CowActivity.windowFocus;
 	   	
 	   	
 	   	if(total == 0){
 	   		timer = new Timer();
 	   		timer.schedule(new MusicTimerTask(), 8000);//This is the amount of buffer time for the music.
 	   	}
 	   	else if(total == 1 && (!MusicMethod.musicStopped) && (!MusicMethod.suffolkShuffle.isPlaying()))
 	   		MusicMethod.suffolkShuffle.start();
 	   	
	}
}
